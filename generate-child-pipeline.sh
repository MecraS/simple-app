#!/bin/sh

echo "Creating child pipeline"

export first_variable=$1

( echo "cat <<EOF > child-pipeline.yml";
  cat child-pipeline-template.yml;
  echo "EOF";
) >temp.sh

chmod +x temp.sh
./temp.sh

echo "Child pipeline yaml is:"
cat child-pipeline.yml